// Guang Zhang 1942372

package movies.importer;

import java.util.*;

/**
 * A class that is extending to the Processor class.
 * @author Guang Zhang 1942372
 *
 */

public class RemoveDuplicates extends Processor {
	
	// Remove Duplicates from the texts without headers
	public RemoveDuplicates(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}
	
	// Remove duplicate strings
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> temp = new ArrayList<String>(input);
		ArrayList<String> asRmDup = new ArrayList<String>();
		for (String s: input) {
			temp.remove(s);
			if (!temp.contains(s)) {
				asRmDup.add(s);
			}
		}
		return asRmDup;
	}
}

