// Guang Zhang 1942372
package movies.importer;

import java.util.*;

/**
 * A class that is extending to the Processor class.
 * @author Guang Zhang 1942372
 *
 */

public class LowercaseProcessor extends Processor {

	public LowercaseProcessor(String sourceDir, String outputDir) {
		// call base class Processor with headers being true
		super(sourceDir, outputDir, true);
	}
	
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> asLower = new ArrayList<String>();
		for (String s: input) {
			asLower.add(s.toLowerCase());
		}
		return asLower;
	}
}
