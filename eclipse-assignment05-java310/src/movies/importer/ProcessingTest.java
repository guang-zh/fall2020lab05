// Guang Zhang 1942372
package movies.importer;

import java.io.*;
import java.util.*;

/**
 * A test class that is testing LowercaseProcessor class.
 * @author Guang Zhang 1942372
 *
 */

public class ProcessingTest {
	
	// Test LowercaseProcessor class headers set to be true
	public static void main(String[] args) throws IOException {
		String source="//Users//guang//Desktop//ProgrammingIII//java310//fall2020lab05//textsSource";
		String destination="//Users//guang//Desktop//ProgrammingIII//java310//fall2020lab05//textsDestination";
		LowercaseProcessor lpObj = new LowercaseProcessor(source, destination);
		lpObj.execute();
		source="//Users//guang//Desktop//ProgrammingIII//java310//fall2020lab05//textsDestination";
		destination="//Users//guang//Desktop//ProgrammingIII//java310//fall2020lab05//textsRemoveDuplicates";
		RemoveDuplicates rdObj = new RemoveDuplicates(source, destination);
		rdObj.execute();
	}
}



